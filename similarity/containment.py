# -*- coding: UTF-8 -*-

__author__ = "Alberto Barron-Cedeno"
__copyright__ = "Copyright 2018, Tiny_SIM"
__credits__ = ["Alberto Barron-Cedeno"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Alberto Barron-Cedeno"
__email__ = "albarron@qf"
__status__ = "Experimentation"

import logging

from representation.bagof import CharNgrams, WordNgrams

logging.basicConfig(level=logging.INFO)


class Containment:

    def __init__(self, words=True, chars=False, sw=True, n=2):
        """
        Initialise setting whether the computation will be carried
        out at word or character level and using which level of
        n-gram.

        Warning: this is not intended to be efficient. It is supposed to
        deal with pairs of texts only. If you require many computations
        with common texts (e.g., two collections so you need nxm),
        contact me.
        TODO this should indeed be "use XX method"

        :param words:
                    True if words are required
        :param chars:
                    True if characters are required
        :param sw:
                    True if the text should be stopworded
        :param n:
                    Level of the n-gram
        """

        if words == chars:
            logging.error("Only one of words or chars can be used")
            exit(-2)

        if words:
            self.representer = WordNgrams(sw, n)
            logging.info("Working with word n-grams")
        else:
            self.representer = CharNgrams(sw, n)
            logging.info("Working with character n-grams")

        logging.info("Level of the n-gram: %i", n)

    def get(self, small_text, large_text):
        """
        Compute containment between the string-based representations
        of two texts. Keep in mind the small text should be actually
        small than the large one. Otherswise, you might consider to
        use standard Jaccard (or perhaps cosine, if frequencies are
        relevant)

        :param small_text:
                    string representation of a text
        :param large_text:
                    string representation of a text
        :return:
                    containment: size_of_intersection/size_of_small_text
        """
        small_tokens = self.representer.get_set_repr(small_text)
        large_tokens = self.representer.get_set_repr(large_text)
        common = len(small_tokens.intersection(large_tokens))
        if len(small_tokens) == 0:
            return 0.0
        return 1.0 * common / len(small_tokens)




