# -*- coding: UTF-8 -*-

__author__ = "Alberto Barron-Cedeno"
__copyright__ = "Copyright 2018, Tiny_SIM"
__credits__ = ["Alberto Barron-Cedeno"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Alberto Barron-Cedeno"
__email__ = "albarron@qf"
__status__ = "Experimentation"

import logging
from nltk import word_tokenize
from nltk.corpus import stopwords



logging.basicConfig(level=logging.INFO)


class Tokens():

    def __init__(self, sw=True):
        """
        Initialize the object setting whether stopwords are going to
        be discarded (default) or not. Stopwording includes discarding
        words shorter than 2 characters (which would discard other elements
        e.g., punctuation)

        :param sw:
                True if stopwords should be discarded (default)
        """
        self.sw = sw
        self.stoplist = set(stopwords.words("english"))

    def get_set_repr(self, text):
        logging.error("Still not implemented")
        exit(-2)

    def get_freq_repr(self, text):
        return self._count_from_list(self._get_tokens(text))

    def _count_from_list(self, list):
        counter = dict()
        for el in list:
            if el not in counter:
                counter[el] = 0
            counter[el] += 1
        return counter

    def _get_tokens(self, text):
        tokens = word_tokenize(text.lower())
        if self.sw:
            tokens = [word for word in tokens if word not in self.stoplist and len(word) > 1]
        return tokens


class WordNgrams(Tokens):

    def __init__(self, sw=True, n=2):
        super(WordNgrams, self).__init__(sw)
        self.n = n

    def get_set_repr(self, text):
        return set(self._get_ngrams(text))

    def get_freq_repr(self, text):
        tokens = self._get_ngrams(text)
        return self._count_from_list(tokens)

    def get_norm_repr(self, text):
        tokens = self.get_freq_repr(text)
        denominator = 1.0 * sum([x for x in tokens.values()])
        for k in tokens.keys():
            tokens[k] /= denominator
        return tokens

    def _get_ngrams(self, text):
        tokens = self._get_tokens(text)
        tokens = [" ".join(tokens[i:i + self.n]) for i in range(0, len(tokens) - self.n + 1)]
        return tokens
    #
    # self.query_tfidf.add_document(self.QID, tokens)

class CharNgrams():

    def __init__(self):
        logging.error("Still not implemented")
        exit(-2)
