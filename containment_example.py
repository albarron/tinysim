# -*- coding: UTF-8 -*-

__author__ = "Alberto Barron-Cedeno"
__copyright__ = "Copyright 2018, Tiny_SIM"
__credits__ = ["Alberto Barron-Cedeno"]
__license__ = "GPL"
__version__ = "0.1"
__maintainer__ = "Alberto Barron-Cedeno"
__email__ = "albarron@qf"
__status__ = "Experimentation"


from similarity.containment import Containment

short_text = ('InSight is a robotic lander designed to study the deep '
              'interior of the planet Mars.'
              )

long_text = ("The mission is managed by the Jet Propulsion Laboratory "
              "for NASA. The lander was manufactured by Lockheed Martin "
              "Space Systems. The name is an acronym for Interior "
              "Exploration using Seismic Investigations, Geodesy and Heat "
              "Transport. InSight's objective is to place a stationary "
              "lander equipped with a seismometer called SEIS produced "
              "by the French space agency CNES, and measure heat transfer "
              "with a heat probe called HP3 produced by the German space "
              "agency DLR to study the planet's early geological evolution. "
              "This could bring new understanding of the Solar System's "
              "terrestrial planets—Mercury, Venus, Earth, Mars—and Earth's "
              "Moon. By reusing technology from the Mars Phoenix lander, "
              "which successfully landed on Mars in 2008, it was hoped that "
              "the cost and risk would be reduced."
              )

empty_text = ""

# cont_identical = sim.get(short_text, short_text)
# cont_short_long = sim.get(short_text, long_text)
# cont_empty_short = sim.get(empty_text, short_text)

print("Computing similarities between various texts...")

print("SHORT:", short_text)
print("LONG:", long_text)
print("EMPTY", empty_text)

sim = Containment()
print("Default parameters: sw=True, n=2")
print("sim(SHORT, SHORT)=", sim.get(short_text, short_text))
print("sim(SHORT, LONG)=", sim.get(short_text, long_text))
print("sim(EMPTY, LONG)=", sim.get(empty_text, short_text))


sim = Containment(sw=False, n=1)
print("Other parameters: sw=False, n=1")
print("sim(SHORT, SHORT)=", sim.get(short_text, short_text))
print("sim(SHORT, LONG)=", sim.get(short_text, long_text))
print("sim(EMPTY, LONG)=", sim.get(empty_text, short_text))

sim = Containment(n=1)
print("Other parameters: sw=True, n=1")
print("sim(SHORT, SHORT)=", sim.get(short_text, short_text))
print("sim(SHORT, LONG)=", sim.get(short_text, long_text))
print("sim(EMPTY, LONG)=", sim.get(empty_text, short_text))

