import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tiny_sim",
    version="0.0.1",
    author="Alberto",
    author_email="albarron@qcri",
    description="A tiny package with a number of similarities",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/albarron/tinysim",
    packages=setuptools.find_packages(),
    install_requires=[
              'nltk',
          ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
)
